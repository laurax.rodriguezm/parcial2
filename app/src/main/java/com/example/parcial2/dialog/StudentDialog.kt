package com.example.parcial2.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.example.parcial2.R
import kotlinx.android.synthetic.main.dialog_student.*

class StudentDialog (context: Context, val name: String, val lastname: String, val document: String, val phone: String, private val callback: (String, String, String, String) -> Unit) : Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_student)
        nameEditText.setText(name)
        lastNameEditText.setText(lastname)
        docEditText.setText(document)
        phonekEditText.setText(phone)

        saveButton.setOnClickListener {
            makeValidation()

        }
    }


    private fun makeValidation(){

        if (nameEditText.text.isEmpty()|| lastNameEditText.text.isEmpty()|| docEditText.text.isEmpty() || phonekEditText.text.isEmpty()){
            val builder= AlertDialog.Builder(context)
            builder.setMessage("Todos los campos son obligatorios")
            builder.setPositiveButton("Aceptar"){ _,_ ->

            }
            builder.show()
        }else {
            val name = nameEditText.text.toString()
            val lastname = lastNameEditText.text.toString()
            val document= docEditText.text.toString()
            val phone= phonekEditText.text.toString()
            callback(name, lastname, document, phone)
            dismiss()

        }

    }
}
