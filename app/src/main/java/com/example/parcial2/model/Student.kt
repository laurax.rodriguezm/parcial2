package com.example.parcial2.model

data class Student (var name: String, var lastname: String, var document: String, var phone: String)