package com.example.parcial2.activity2.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.parcial2.R
import kotlinx.android.synthetic.main.fragment_one.*
import kotlinx.android.synthetic.main.fragment_second.*


class SecondFragment : Fragment() {
    var image1=true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)

        editImageButton.setOnClickListener {
            image1=!image1
            changeImage()

        }
    }

    private fun changeImage(){
        if(image1){
            imageView.setImageResource(R.drawable.moon)
        }else{
            imageView.setImageResource(R.drawable.sun)
        }


    }
}