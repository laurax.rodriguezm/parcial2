package com.example.parcial2.activity2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentTransaction
import com.example.parcial2.R
import com.example.parcial2.activity2.fragments.OneFragment
import com.example.parcial2.activity2.fragments.SecondFragment
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)


        oneFragmentButton.setOnClickListener {
            val transaction: FragmentTransaction= supportFragmentManager.beginTransaction()
            transaction.add(R.id.container, OneFragment(), null)
            transaction.commit()
        }

        twoFragmentButton.setOnClickListener {
            val transaction: FragmentTransaction= supportFragmentManager.beginTransaction()
            transaction.add(R.id.container, SecondFragment(), null)
            transaction.commit()
        }





    }
}