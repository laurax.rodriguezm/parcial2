package com.example.parcial2.activity2.fragments

import android.app.TimePickerDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.parcial2.R
import kotlinx.android.synthetic.main.fragment_one.*


class OneFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_one, container, false)


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)

        editTextViewButton.setOnClickListener {
            makeTimeDialog()

        }
    }

    private fun makeTimeDialog(){
        val dialog= TimePickerDialog(requireContext(), {dialog,hour,minute ->
            textViewEdit.setText(hour.toString().plus(":").plus(minute.toString()))

        },9, 28, true)
        dialog.show()
    }


}