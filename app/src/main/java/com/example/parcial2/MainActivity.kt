package com.example.parcial2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.parcial2.activity2.SecondActivity
import com.example.parcial2.dialog.StudentDialog
import com.example.parcial2.model.Student
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var adapter: StudentAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupButtons()
        setupList()
        nextActivityButton.setOnClickListener {
            val intent= Intent(this, SecondActivity:: class.java )
            startActivity(intent)

        }
    }

    private fun setupButtons() {
        addButton.setOnClickListener {
            val dialog = StudentDialog(this, "", "", "", "") { name, lastname, document, phone ->
                addStudent(name, lastname, document, phone)
            }
            dialog.show()
        }
    }

    private fun setupList() {
        val students = mutableListOf(
            Student("Laura Ximena", "Rodriguez Machado", "1020797497", "3115232941"),
            Student("Yorladis", "Grisales Henao", "1020770512", "3132665419"),
            Student("Yamile Yohana", "Lagares Arroyo", "10205215512", "3213981031"))

        adapter = StudentAdapter(students) { item, isDelete ->
            if(isDelete) deleteStudent(item)

        }
        productRecyclerView.adapter = adapter
        productRecyclerView.layoutManager = LinearLayoutManager(this)
    }

    private fun addStudent(name: String, lastname: String, document: String, phone: String) {
        val student = Student(name, lastname, document, phone)
        adapter.addStudent(student)
        adapter.notifyDataSetChanged()
    }

    private fun deleteStudent(student: Student) {

        val builder= AlertDialog.Builder(this)
        builder.setMessage("Seguro desea eliminar informació de estudiante")
        builder.setPositiveButton("Aceptar"){ _,_ ->
            adapter.deleteStudent(student)
            adapter.notifyDataSetChanged()

        }
        builder.setNegativeButton("Cancelar") { dialog, position ->
        }
        builder.show()

    }


}