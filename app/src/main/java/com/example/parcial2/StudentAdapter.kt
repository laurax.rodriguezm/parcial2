package com.example.parcial2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.parcial2.model.Student
import kotlinx.android.synthetic.main.dialog_student.view.*
import kotlinx.android.synthetic.main.item_student.view.*

class StudentAdapter (val students: MutableList<Student>, val callback: (Student, Boolean) -> Unit): RecyclerView.Adapter<StudentAdapter.ProductViewHolder>() {

    class ProductViewHolder(itemView: View, val callback: (Student, Boolean) -> Unit) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Student) {
            itemView.nameTextView.text = item.name
            itemView.lastNameTextView.text = item.lastname
            itemView.documentTextView.text = item.document
            itemView.phoneTextView.text = item.phone

            itemView.deleteButton.setOnClickListener {
                callback(item, true)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_student, parent, false)
        return ProductViewHolder(view, callback)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind(students[position])
    }

    override fun getItemCount(): Int {
        return students.size
    }

    fun addStudent(student: Student) {
        students.add(student)
    }

    fun deleteStudent(student: Student) {

        students.remove(student)
    }

}